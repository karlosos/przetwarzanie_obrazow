import numpy as np
import matplotlib.pyplot as mp
import matplotlib.image as img
import os

m3 = np.random.rand(2,3)
print m3[1,2]

# rozmiar
m3.shape
m3.shape[0]

m4 = np.random.rand(2,3)
m3 * m4 # mnozenie .*

m3.dot(np.transpose(m4)) #normalne mnozenie

mp.plot()
mp.show()

os.getcwd()

obr = img.imread('obrazek.png')
mp.imshow(obr)

obr2 = obr[:,:,0]

mp.imshow(obr2, cmap='hot')

mp.show()