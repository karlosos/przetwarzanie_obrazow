# kontrast i jasnosc obrazu

# funkcja liczaca jasnosc obrazka o rozmiarze MxN
# wartosc srednia J = 1/MN suma(f,x) - dodac wszystkie wartosci i podzielic przez liczbe
# kontrast - sqrt(1/MN * suma(f(x,y)-J)^2)

# obr2 = obr + a -> liczymy jasnosc i kontrast i pokazujemy wykres. czy dodawanie do obrazka stalej wartosci
# pozwala na zmiane kontrastu janosci itd

# obr * a i wykres J y od a x
# obr ^ a = niecalkowite a

# sprawdzic jak zwiekszyc int o milion

import numpy as np
import matplotlib.pyplot as mp
import matplotlib.image as img
import matplotlib.pyplot as plt
import matplotlib
import math
import sys

def brightness(obr):
    suma = 0
    w = obr.shape[0]
    k = obr.shape[1]
    suma = np.sum(obr[:,:,0])
    return (suma/(w * k))

def contrast(obr):
    suma = 0
    w = obr.shape[0]
    k = obr.shape[1]
    bright = brightness(obr)
    # tworze kopie bo samo obr2 = obr to tylko referencja na ta sama tablice
    obr_kontrastu = obr.copy()
    obr_kontrastu = (obr_kontrastu-bright)**2
    return(np.sqrt(obr_kontrastu.mean()))

def dodawanie(obr, x):
    contr = np.zeros(x.shape[0])
    brig = np.zeros(x.shape[0])

    l = 0
    for i in x:
        obr2 = obr.copy() + i
        obr2[obr2 > 255] = 255
        j = brightness(obr2)
        k = contrast(obr2)
        brig[l] = j
        contr[l] = round(k, 4);
        l += 1

    return(brig,contr)

def mnozenie(obr, x):
    contr = np.zeros(x.shape[0])
    brig = np.zeros(x.shape[0])

    l = 0
    for i in x:
        obr2 = obr.copy() * i
        #obr2[obr2 > 255] = 255
        j = brightness(obr2)
        k = contrast(obr2)
        brig[l] = j
        contr[l] = round(k, 4);
        l += 1

    return(brig,contr)

def potegowanie(obr, x):
    contr = np.zeros(x.shape[0])
    brig = np.zeros(x.shape[0])

    l = 0
    for i in x:
        obr2 = np.power(obr, i)
        #obr2[obr2 > 255] = 255
        j = brightness(obr2)
        k = contrast(obr2)
        brig[l] = j
        contr[l] = round(k, 4);
        l += 1

    return(brig,contr)

def pierwiastkowanie(obr, x):
    contr = np.zeros(x.shape[0])
    brig = np.zeros(x.shape[0])

    l = 0
    for i in x:
        obr2 = np.power(obr, 1.0/i)
        #obr2[obr2 > 255] = 255
        j = brightness(obr2)
        k = contrast(obr2)
        brig[l] = j
        contr[l] = round(k, 4);
        l += 1

    return(brig,contr)

def logarytmowanie(obr, x):
    contr = np.zeros(x.shape[0])
    brig = np.zeros(x.shape[0])

    l = 0
    for i in x:
        obr2 = np.divide(np.log(obr),np.log(i))
        #obr2[obr2 > 255] = 255
        j = brightness(obr2)
        k = contrast(obr2)
        brig[l] = j
        contr[l] = round(k, 4);
        l += 1

    return(brig,contr)

def main():
    obr = img.imread('kierowca.png')
    obr = obr*255;
    print("Jasnosc " + str(brightness(obr)))
    print("Contrast " + str(contrast(obr)))

    # dodawanie
    x = np.linspace(0, 100, 10)
    dod = dodawanie(obr, x)
    wyk = plt.subplot(3, 2, 1)
    plt.plot(x, dod[0])
    plt.plot(x, dod[1])
    plt.title('Dodawanie')

    # mnozenie
    x = np.linspace(1, 2, 5)
    mno = mnozenie(obr, x)
    wyk = plt.subplot(3, 2, 2)
    plt.plot(x, mno[0])
    plt.plot(x, mno[1])
    plt.title('Mnozenie')

    # potegowanie
    x = np.linspace(1, 1.3, 4)
    pot = potegowanie(obr, x)
    wyk = plt.subplot(3, 2, 3)
    plt.plot(x, pot[0])
    plt.plot(x, pot[1])
    plt.title('Potegowanie')

    #pierwiastkowanie
    x = np.linspace(1, 3, 10)
    pier = pierwiastkowanie(obr, x)
    wyk = plt.subplot(3, 2, 4)
    plt.plot(x, pier[0])
    plt.plot(x, pier[1])
    plt.title('Pierwiastkowanie')

    #logarytmowanie
    x = np.linspace(1.2, 3, 5)
    loga = logarytmowanie(obr, x)
    plt.subplot(3, 2, 5)
    l1 = plt.plot(x, loga[0], label="test1")
    l2 = plt.plot(x, loga[1], label="test2")
    plt.title('Logarytmowanie')

    plt.subplots_adjust(left=None, bottom=None, right=None, top=None,
                    wspace=None, hspace=0.5)
    #plt.legend((l1, l2), ('Jasnosc', 'Kontrast'), loc=(0.5, 0), ncol=5)
    plt.show()

if __name__ == "__main__":
    main()