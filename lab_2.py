import numpy as np
import matplotlib.image as img
import matplotlib.pyplot as plt

def histogram(obr, ilosc_przedzialow):
    hist = np.zeros(ilosc_przedzialow)
    krok = 1.0/ilosc_przedzialow
    w = obr.shape[0]
    k = obr.shape[1]
    for x in range(0, w):
        for y in range(0, k):
            z = obr[x, y, 0]
            for i in range(0, ilosc_przedzialow):
                if z <= krok*(i+1) and z > krok*(i):
                    hist[i] = hist[i]+1;
    return hist

# refactored faster histogram function
def histogram2(obr, ilosc_przedzialow):
    obr = obr[:, :, 0]
    hist = np.zeros(ilosc_przedzialow)
    krok = 255.0/ilosc_przedzialow

    for i in range(0, ilosc_przedzialow):
        counts1 = obr <= krok*(i+1)
        counts2 = obr > krok*i
        counts = np.logical_and(counts1, counts2)
        print np.sum(counts)
        hist[i] = np.sum(counts)

    return hist

def przygotuj_lut(ilosc_przedzialow, vmax, vmin):
    krok = 1.0 / ilosc_przedzialow
    lut = np.zeros(ilosc_przedzialow)
    for i in range(0, ilosc_przedzialow):
        lut[i] = (1.0/(vmax-vmin))*(i-vmin)
    return lut

def rozciagnij(obr, ilosc_przedzialow, lut):
    krok = 255.0 / ilosc_przedzialow
    w = obr.shape[0]
    k = obr.shape[1]
    for x in range(0, w):
        for y in range(0, k):
            z = obr[x, y, 0]
            for i in range(0, ilosc_przedzialow):
                if z <= krok * (i + 1) and z > krok * (i):
                    obr[x,y,0] = lut[i]
    return obr

def rozciagnij_liniowo(obr, a, b):
    obr2 = a*obr+b
    obr2[obr2>255] = 255
    return obr2

def funkcja_kwadrat(obr):
    obr2 = np.power(obr, 2)
    obr2[obr2>255] = 255
    return obr2

def funkcja_log(obr):
    obr2 = np.log(obr)
    obr2[obr2>255] = 255
    return obr2

def funkcja_pierw(obr):
    obr2 = np.sqrt(obr)
    obr2[obr2>255] = 255
    return obr2

def funkcja_wykklad(obr, a):
    obr2 = np.power(a, obr)
    obr2[obr2>255] = 255
    return obr2

def main():
    obr = img.imread('kierowca.png')
    obr = obr * 255
    ilosc_przedzialow = 20
    x = np.linspace(0, 1, ilosc_przedzialow)

    # zwykly histogram
    hist = histogram2(obr, ilosc_przedzialow)
    plt.plot(x, hist)

    # rozciagniecie liniowe
    obr2 = rozciagnij_liniowo(obr, 2.5, -150)
    hist = histogram2(obr2, ilosc_przedzialow)
    plt.plot(x, hist)

    # kwadrat
    obr3 = funkcja_kwadrat(obr)
    hist = histogram2(obr3, ilosc_przedzialow)
    plt.plot(x, hist)

    # kwadrat
    obr4 = funkcja_pierw(obr)
    hist = histogram2(obr4, ilosc_przedzialow)
    plt.plot(x, hist)

    # log
    obr5 = funkcja_log(obr)
    hist = histogram2(obr5, ilosc_przedzialow)
    plt.plot(x, hist)

    # wyklad
    obr6 = funkcja_wykklad(obr, 2)
    hist = histogram2(obr6, ilosc_przedzialow)
    plt.plot(x, hist)


    plt.show()



    return 0

if __name__ == "__main__":
    main()